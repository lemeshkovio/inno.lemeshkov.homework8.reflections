package inno.lemeshkov;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

class CleanerTest {

    private final Cleaner cleaner = new Cleaner();
    private final Set<String> fieldsToClean = new HashSet<>();
    private final Set<String> fieldsToOut = new HashSet<>();
    private final Person person = new Person();

    @BeforeEach
    void setUp() {
        fieldsToClean.add("name");
        fieldsToClean.add("sex");
        fieldsToClean.add("age");
        fieldsToOut.add("count");
        fieldsToOut.add("color");
        fieldsToOut.add("age");
        person.setAge(25);
        person.setColor(Color.WHITE);
        person.setCount(12000);
        person.setName("Legion");
    }

    @Test
    void test_cleanUp_checkPrintSet() {
        List<String> expect = new ArrayList<>();
        expect.add("field 'count' = 12000");
        expect.add("field 'color' = white");
        expect.add("field 'age' = 25");
        cleaner.cleanUp(person, fieldsToClean, fieldsToOut);
        List<String> result = new ArrayList<>(cleaner.fieldsToPrint);
        for (String str : expect) {
            assertTrue(result.contains(str));
        }
    }

    @Test
    void test_cleanUp_checkSetFieldsToDefault(){
        List<String> expect = new ArrayList<>();
        expect.add("null");
        expect.add("false");
        expect.add("0");
        cleaner.cleanUp(person, fieldsToClean, fieldsToOut);
        List<String> result = new ArrayList<>();
        result.add(String.valueOf(person.getAge()));
        result.add(String.valueOf(person.getName()));
        result.add(String.valueOf(person.isSex()));
        for (String str : expect) {
            assertTrue(result.contains(str));
        }
    }
}