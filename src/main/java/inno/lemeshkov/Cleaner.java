package inno.lemeshkov;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class Cleaner {
    public List<String> fieldsToPrint = new ArrayList<>();
    private List<String> types = Arrays.asList("byte","short","char","int","long","float","double");

    public void cleanUp(Object object, Set<String> fieldsToClean, Set<String> fieldsToOut) {
        fillPrintList(object, fieldsToOut);
        setDefaultValue(object, fieldsToClean);
        fieldsToPrint.forEach(System.out::println);
    }

    private void setDefaultValue(Object object, Set<String> fieldsToClean) {
        try{
            Class clazz;
            clazz = Class.forName(object.getClass().getName());
            Field[] fieldList = clazz.getDeclaredFields();
            for (String fieldName : fieldsToClean) {
                for (Field field : fieldList) {
                    if (field.getName().equals(fieldName)) {
                        field.setAccessible(true);
                        if (types.contains(field.getType().toString())){
                            field.set(object, 0);
                        }else if(field.getType().toString().equals("boolean")){
                            field.setBoolean(object,false);
                        } else {
                            field.set(object, null);
                        }
                    }
                }
            }
        }catch (Throwable e){
            System.err.println("Can't set fields to default value\n");
            e.printStackTrace();
        }
    }

    private void fillPrintList(Object object, Set<String> fieldsToOut) {
        try {
            Class clazz = Class.forName(object.getClass().getName());
            Field[] fieldList = clazz.getDeclaredFields();
            for (String fieldName : fieldsToOut) {
                for (Field field : fieldList) {
                    if (field.getName().equals(fieldName)) {
                        field.setAccessible(true);
                        fieldsToPrint.add(String.format("field '%s' = %s",fieldName, field.get(object).toString()));
                    }
                }
            }
        } catch (Throwable e) {
            System.err.println("Can't add fields to PrintList\n");
            e.printStackTrace();
        }
    }
}
