package inno.lemeshkov;

public enum Color {
    WHITE("white"), BLACK("black"), RED("red"), YELLOW("yellow");

    public String getDescription() {
        return description;
    }

    private final String description;

    Color(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.description;
    }


}
